//! This module provides an adapter to the logging port for use with the
//! [tracing](https://docs.rs/tracing) library. When used, all logging statements will become
//! tracing statements, which will all be emitted to stdout in a format matching that used by
//! [env_logger](https://docs.rs/env_logger/).
//!
//! Practically speaking, all this crate really does is convert the `TpfsLogCfg` provided by
//! the logging port into parameters to the tracing [FmtSubscriber](https://docs.rs/tracing-subscriber/0.2.4/tracing_subscriber/fmt/struct.Subscriber.html)

#![forbid(unsafe_code)]

use std::io;
use tpfs_logger_port::{log, LogError, TpfsLogCfg};
use tracing_subscriber::filter::Directive;
use tracing_subscriber::{filter::LevelFilter, EnvFilter, FmtSubscriber};

/// Initializes the logger with default configuration values.
///
/// # Errors
/// An error is returned if the initialization fails.
pub fn init_with_default_config() -> Result<(), LogError> {
    init(&TpfsLogCfg::default())
}

/// Initializes the logger.
///
/// # Errors
/// An error is returned if the initialization fails.
pub fn init(config: &TpfsLogCfg) -> Result<(), LogError> {
    let builder = FmtSubscriber::builder()
        .with_writer(io::stderr)
        .with_env_filter(cfg_levels_to_env_filter(config));

    // We suffer some dupe inside the if because `json` turns the builder into a different
    // type, there's no trait.
    if TpfsLogCfg::human_readable_mode() {
        builder
            .try_init()
            .map_err(|e| LogError::InitializationError { source: e })
    } else {
        builder
            .json()
            // Flatten k/v pairs and "message" field to the top of the emitted json
            .flatten_event(true)
            .try_init()
            .map_err(|e| LogError::InitializationError { source: e })
    }
}

// Could technically pull in the entire tracing-log crate for this, but that seems heavy.
fn log_level_to_trace_level(l: log::LevelFilter) -> LevelFilter {
    match l {
        log::LevelFilter::Off => LevelFilter::OFF,
        log::LevelFilter::Error => LevelFilter::ERROR,
        log::LevelFilter::Warn => LevelFilter::WARN,
        log::LevelFilter::Info => LevelFilter::INFO,
        log::LevelFilter::Debug => LevelFilter::DEBUG,
        log::LevelFilter::Trace => LevelFilter::TRACE,
    }
}

fn cfg_levels_to_env_filter(config: &TpfsLogCfg) -> EnvFilter {
    // This is... silly. See https://github.com/tokio-rs/tracing/issues/404
    let mut env_filter = EnvFilter::default();
    for level in &config.target_to_level {
        let level_string = format!("{}={}", level.0, level.1);
        if let Ok(d) = level_string.parse() {
            env_filter = env_filter.add_directive(d);
        }
    }
    // Also set the default level
    env_filter = env_filter.add_directive(Directive::from(log_level_to_trace_level(
        config.default_level,
    )));
    env_filter
}

#[cfg(test)]
mod test {
    use crate::init_with_default_config;
    use tpfs_logger_port::*;
    use tracing::{span, Level};

    #[logging_event]
    pub enum TestE {
        Evt(String),
    }

    #[test]
    fn default_settings_make_sense() {
        // this test is really more to manually visually check the output
        // std::env::set_var(HUMAN_READABLE_ENV_VAR, "true");
        init_with_default_config().unwrap();
        trace!(TestE::Evt("trace!".to_string()));
        debug!(TestE::Evt("debug!".to_string()));
        info!(TestE::Evt("info!".to_string()));
        warn!(TestE::Evt("warn!".to_string()));
        error!(TestE::Evt("error!".to_string()));

        // Test with a span
        let span = span!(Level::INFO, "spanny");
        let _span = span.enter();
        info!(TestE::Evt("in da span yo".to_string()));
        drop(_span);
        let span = span!(Level::INFO, "other span");
        let _span = span.enter();
        tracing::info!("tracing info in span");
        drop(_span);
    }
}
